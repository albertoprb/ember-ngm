var express = require('express');
var path = require('path');

var app = express();
app.use(express.static(path.join(__dirname, '../public')));


app.listen(process.env.PORT, function() {
    console.log('Server started on port ' + process.env.PORT);
});


