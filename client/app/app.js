window.App = Ember.Application.create({
	ready: function() {
		console.log('Ember application is ready');
	}
});

window.App.ApplicationController = Ember.Controller.extend({
	init: function() {
		console.log('ApplicationController.init');
		this._super.apply(this, arguments);
	}
});

window.App.ApplicationView = Ember.View.extend({
	init: function() {
		console.log('ApplicationView.init');
		this._super.apply(this, arguments);
	}
});

function ev(emberId){
    var result = Ember.View.views['ember' + emberId];
    console.log(result.toString());
    return result;
}
function ec(emberId){
    var result = Ember.View.views['ember' + emberId].get('controller');
    console.log(result.toString());
    return result;
}

window.ev = ev;
window.ec = ec;

window.App.IndexController = Ember.Controller.extend({
	init: function() {
		console.log('IndexController.init');
		this._super.apply(this, arguments);
	}
});

window.App.IndexView = Ember.View.extend({
	init: function() {
		console.log('IndexView.init');
		this._super.apply(this, arguments);
	}
});

window.App.Menu1View = Ember.View.extend({
	init: function() {
		console.log('Menu1View.init');
		this._super.apply(this, arguments);
	},
	destroy: function() {
		console.log('Menu1View.destroy');
		this._super.apply(this, arguments);	
	},
	didInsertElement: function() {
		console.log('Menu1View.didInsertElement');

		this.$("li").fadeOut();

		this._super.apply(this, arguments);		
	}
});

window.App.Menu2View = Ember.View.extend({
	init: function() {
		console.log('Menu2View.init');
		this._super.apply(this, arguments);
	}
});






window.App.AdminView = Ember.View.extend({
	init: function() {
		console.log('AdminView.init');
		this._super.apply(this, arguments);
	}
});

window.App.AdminOverviewView = Ember.View.extend({
	init: function() {
		console.log('AdminOverviewView.init');
		this._super.apply(this, arguments);
	}
})

window.App.Router.map(function() {

	this.route('index', {path: '/'});
	this.route('menu1', {path:'/menu1'});
	this.route('menu2', {path:'/menu2'});
	this.route('superfreakythingie', {path:'/asdföadsfglökafjlgk'});

	this.resource('admin', {path:'/admin'}, function() {
		this.route('overview');
	});



});


