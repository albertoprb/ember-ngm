var matchdep = require('matchdep');
var path = require('path');
var fs = require('fs');

module.exports = function(grunt) {
    'use strict';

    // load all grunt plugins from node_modules folder
    matchdep.filterAll('grunt-*').forEach(grunt.loadNpmTasks);

    grunt.initConfig({
        express: {
            dev: {
                options: {
                    script: 'server/server.js'
                }
            }
        },

        neuter : {
            options : {
                includeSourceURL: true
            },
            app : {
                src : 'client/app/**/*.js',
                dest: 'public/assets/js/main.js'
            }
        },

        less : {
            uncompressed : {
                files : {
                    'public/assets/css/main.css' : 'client/less/main.less'
                }
            }
        },

        concat : {
            vendorjs : {
                // generate warnings if input files are not found
                nonull:true,

                dest: 'public/assets/js/vendor.js',
                src : [
                    'bower_components/jquery/jquery.js',
                    'bower_components/lodash/lodash.js',
                    'bower_components/handlebars/handlebars.js',
                    'bower_components/ember/ember.js'
                ]
            }
        },

        ember_handlebars : {
            compile : {
                options: {
                    namespace: 'Ember.TEMPLATES',
                    processName: function(name) {
                        var name = path.basename(name);
                        // strip extension from file
                        name = name.substr(0, name.lastIndexOf('.'));
                        return name;
                    },

                    processPartialName : function(name) {
                        var name = path.basename(name);
                        // strip extension from file
                        name = name.substr(0, name.lastIndexOf('.'));
                        return name;
                    }
                },
                files : {
                    './public/assets/js/templates.js': 'client/app/**/*.hbs'
                }
            }
        },

        watch: {
            options: {
                livereload: true,
                nospawn:true
            },

            server: {
                files:  [
                    'server/**/*.js'
                ],
                tasks: ['express']
            },

            less : {
                files : [
                    'client/less/**/*.less'
                ],
                tasks : ['less']
            },
            
            client: {
                files:  [
                    'client/app/**/*.js'
                ],
                tasks:  ['neuter']
            },

            templates: {
                files:  [
                    'client/app/**/*.hbs'
                ],
                tasks: ['ember_handlebars']
            },

            html : {
                files : [
                    'public/**/*.html'
                ]
            }
        }
    });

    grunt.registerTask('default', ['less', 'concat', 'neuter', 'ember_handlebars', 'express', 'watch']);
};
